#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

mkdir -p $HOME/.local/share/icons &>/dev/null

cp -f $SCRIPT_DIR/icons/brave-browser-128.png $HOME/.local/share/icons/

cp -f $SCRIPT_DIR/resources/run.sh $HOME/.local/bin/brave_podman_run.sh

chmod 700 $HOME/.local/bin/brave_podman_run.sh

cat << EOF > $HOME/.local/share/applications/brave_podman.desktop
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Brave Podman
Comment=Web Browser
Icon=$HOME/.local/share/icons/brave-browser-128.png
Exec=$HOME/.local/bin/brave_podman_run.sh
Terminal=false
Categories=Tags;Describing;Application
EOF

chmod 600 $HOME/.local/share/icons/brave-browser-128.png $HOME/.local/share/applications/brave_podman.desktop

cp -f $SCRIPT_DIR/resources/chrome_seccomp.json $HOME/.config/

chmod 600 $HOME/.config/chrome_seccomp.json
